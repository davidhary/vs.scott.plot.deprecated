# Scott Plot Project

Support Charting using a custom chart control.

## Getting Started

Clone the project along with its requisite projects to its respective relative path:
```
.\Libraries\VS\Visualizing\Scott
git clone git@bitbucket.org:davidhary/vs.scott.plot.git
```

### Installing

Install the project into the following folders:

## Testing

The project includes a few unit test classes. Test applications are under the *Demos* solution folder. 

## Built, Tested and Facilitated By

* [Visual Studio](https://www.visualstudIO.com/) - Visual Studio 2015
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - Wix Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker

## Authors

* [ScottPlot v3](https://github.com/swharden/ScottPlot/) (May 2019 - present)

## License

This project is licensed under the [MIT License](https://www.bitbucket.org/davidhary/vs.scott.plot/src/master/LICENSE.md)

## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow](https://www.stackoveflow.com)

## Revision Changes

* [ScottPlot v3](https://github.com/swharden/ScottPlot/) (May 2019 - present)
